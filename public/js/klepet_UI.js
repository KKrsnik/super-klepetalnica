/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
 var stevecSlik = 0;
 var steviloSlik = 0;
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = (sporocilo.indexOf("http") > -1 && 
                (sporocilo.indexOf(".jpg") > -1 || sporocilo.indexOf(".png") > -1 || sporocilo.indexOf(".gif") > -1));
   
 if (sporocilo.indexOf(".jpg") > -1) {
    steviloSlik += (sporocilo.match(/.jpg/g) || []).length;
    console.log(steviloSlik);
  }
  if (sporocilo.indexOf(".png") > -1) {
    steviloSlik += (sporocilo.match(/.png/g) || []).length;
  }
  if (sporocilo.indexOf(".gif") > -1) {
    steviloSlik += (sporocilo.match(/.gif/g) || []).length;
  }
  var jeKrcanje = sporocilo.indexOf("&#9756;") > -1;
  
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if(jeSlika) {
    $('#sporocila').append($('<div style="font-weight: bold"></div>').text(sporocilo));
    var razkosanoSporocilo = sporocilo
    for (var ind = 0; ind < steviloSlik; ind++) {
    var zacetekSlike = razkosanoSporocilo.search('http');
    if (razkosanoSporocilo.indexOf(".jpg") > -1) {
      var konecSlike = razkosanoSporocilo.search('.jpg') + 4;
    } else if (razkosanoSporocilo.indexOf(".png") > -1) {
      konecSlike = razkosanoSporocilo.search('.png') + 4;
    } else if (razkosanoSporocilo.indexOf(".gif") > -1) {
      konecSlike = razkosanoSporocilo.search('.gif') + 4;
    }
    var src = razkosanoSporocilo.substring(zacetekSlike, konecSlike);
    $('#sporocila').append('</br><div id=slika' + stevecSlik + '><div>')
    pokaziSliko(src, '#slika'+stevecSlik);
    stevecSlik++;
    razkosanoSporocilo = razkosanoSporocilo.replace(src, '');
    }
  }
   else if(jeKrcanje) {
    sporocilo = sporocilo.split("<").join("&lt;").split(">").join("&gt;");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
  steviloSlik = 0;
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  var jeKrcanje = sporocilo.indexOf("&#9756;") > -1;
  if (jeKrcanje) {
    return $('<div></div>').html('<b>' + sporocilo + '</b>');
  } else {
    return $('<div></div>').html('<i>' + sporocilo + '</i>');  
  }
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

function pokaziSliko(src, div) {
  $(div).html('<img src="' + src + '" />');
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    if (sporocilo.navadno) {
      var zapisiNadimek = nadimek(sporocilo.nadimek);
      var novElement = divElementEnostavniTekst(zapisiNadimek + sporocilo.besedilo);
    } else {
      novElement = divElementEnostavniTekst(sporocilo.besedilo);
    }
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });

  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimek(uporabniki[i])));
    }
   $('#seznam-uporabnikov div').click(function() {
      var vzdevekZNadimkom = $(this).text();
      var sporocilo = klepetApp.procesirajUkaz('/zasebno "' + reverseNadimek(vzdevekZNadimkom) + '" "&#9756;"');
      $('#sporocila').append($('<div></div>').html('<b>' + sporocilo + '</b>'));
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  function nadimek(uporabnik) {
    for (var j in klepetApp.nadimki()) {
        if (j === uporabnik) {
          return klepetApp.nadimki()[j];
        }
      }
      return uporabnik
  }
  
  function reverseNadimek(uporabnik) {
    for (var j in klepetApp.reverseNadimki()) {
        if (j === uporabnik) {
          return klepetApp.reverseNadimki()[j];
        }
      }
      return uporabnik
  }
  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
